const express = require("express");
const app = express();

const port = process.env.PORT || 5000;

app.get("/", (req, res) => {
  return res.status(200).send({
    message: "Hello World!",
  });
});

app.get("/user-info", (req, res) => {
  return res.status(200).send(
    [
      {
        name: "adminUser",
        type: "admin",
        info: "Has the previlage to access all the information"
      },
      {
        name: "maddy",
        type: "user",
        info: "Has the previlage to access certaine information"
      },,
    ]
  );
});

app.listen(port, () => {
  console.log("Listening on " + port);
});

module.exports = app;
