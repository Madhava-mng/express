import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
  vus: 300,
  duration: '10s'
}
export default function() {
  http.get('http://localhost:5000/');
  sleep(1)
  http.get('http://localhost:5000/user-info');
  sleep(1);
}
